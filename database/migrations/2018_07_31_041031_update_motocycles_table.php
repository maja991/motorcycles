<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMotocyclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('motocycles', function (Blueprint $table) {
            $table->text('popis');
            $table->text('cennik');
            $table->text('vykon');
            $table->text('vyska_sedadla');
            $table->text('vodicske_opravnenie');
            $table->text('kaucia');
            $table->text('poznamka');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('motocycles', function (Blueprint $table) {
            $table->dropColumn('popis');
            $table->dropColumn('cennik');
            $table->dropColumn('vykon');
            $table->dropColumn('vyska_sedadla');
            $table->dropColumn('vodicske_opravnenie');
            $table->dropColumn('kaucia');
            $table->dropColumn('poznamka');
        });
    }
}
