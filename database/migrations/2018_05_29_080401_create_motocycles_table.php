<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotocyclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motocycles', function (Blueprint $table) {
            $table->increments('id');
            $table->text('znacka');
            $table->text('model');
            $table->text('typ');
            $table->text('hmotnost');
            $table->text('typ_motora');
            $table->text('objem_motora');
            $table->text('prevodovka');
            $table->text('max_rychlost');
            $table->text('objem_nadrze');
            $table->text('dojazd');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motocycles');
    }
}
