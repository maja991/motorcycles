<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('prislusenstvo1');
            $table->boolean('prislusenstvo2');
            $table->boolean('prislusenstvo3');
            $table->boolean('prislusenstvo4');
            $table->boolean('prislusenstvo5');
            $table->boolean('prislusenstvo6');
            $table->boolean('prislusenstvo7');
            $table->boolean('prislusenstvo8');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('prislusenstvo1');
            $table->dropColumn('prislusenstvo2');
            $table->dropColumn('prislusenstvo3');
            $table->dropColumn('prislusenstvo4');
            $table->dropColumn('prislusenstvo5');
            $table->dropColumn('prislusenstvo6');
            $table->dropColumn('prislusenstvo7');
            $table->dropColumn('prislusenstvo8');
        });
    }
}
