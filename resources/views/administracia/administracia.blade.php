@extends ('layouts.master')
@section ('content')


<div class="content-area">

        <!-- BREADCRUMBS -->
        <section class="page-section breadcrumbs text-right">
            <div class="container">
                <div class="page-header">
                    <h1>Administrácia</h1>
                </div>
                <ul class="breadcrumb">
                    <li><a href="/">Domov</a></li>
                    <li><a href="/administracia">Administrácia</a></li>
                </ul>
            </div>
        </section>
        <!-- /BREADCRUMBS -->

		<div class="col-sm-10 blog-main">
	        <nav class="navigation closed clearfix">
	            <div class="swiper-wrapper">
	                <div class="swiper-slide">
	                    <!-- navigation menu -->
	                    <a href="#" class="menu-toggle-close btn"><i class="fa fa-times"></i></a>
	                    <ul class="nav sf-menu">
	                    	<li><a href="/administracia/zoznam_objednavok">Zoznam objednávok</a></li>
	                    	<li><a href="/administracia/pridaj_motorku">Pridať motorku</a></li>
	                    	<li><a href="/administracia/pridaj_trasu">Pridať trasu</a></li>
	                    	<li><a href="/administracia/zoznam_pouzivatelov">Zoznam používateľov</a></li>
	                	</ul>
	            	</div>
	            </div>
	        </nav>
        </div>

        @yield ('administracia_content')

@endsection