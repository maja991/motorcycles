@extends ('administracia.administracia')
@section ('administracia_content')

<div class="container">
    <div class="row">
 <!-- Zoznam objednavok -->
 	<table class="table table-bordered table-striped table-hover table-responsive">
 		<tr>
 			<td>Id objednávky</td>
 			<td>Meno zákoznáka</td>
 			<td>Priezvisko zákoznáka</td>
 			<td>Email</td>
 			<td>Telefón</td>
 			<td>Adresa pristavenia</td>
 			<td>Adresa vrátenia</td>
 			<td>Id motorky</td>
 		</tr>
		    @foreach ($orders->reverse() as $order)
		    	@include ('administracia.objednavka')
		    @endforeach
	</table>
 <!-- / Zoznam objednavok -->    
 </div>
 </div> 

                    <!-- SIDEBAR -->
                    {{-- @include ('layouts.sidebar') --}}
                        
                    <!-- /SIDEBAR -->


@endsection