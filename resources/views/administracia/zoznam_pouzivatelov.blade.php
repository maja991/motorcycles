@extends ('administracia.administracia')
@section ('administracia_content')

<div class="container">
    <div class="row">
 <!-- Zoznam objednavok -->
 	<table class="table table-bordered table-striped table-hover table-responsive">
 		<tr>
 			<td>Id používateľa</td>
 			<td>Meno používateľa</td>
 			<td>Email</td>
 			<td>Typ používateľa</td>
 			<td></td>
 		</tr>
		    @foreach ($users->reverse() as $user)
		    	@include ('administracia.pouzivatel')
		    @endforeach
	</table>
 <!-- / Zoznam objednavok -->    
 </div>
 </div> 

                    <!-- SIDEBAR -->
                    {{-- @include ('layouts.sidebar') --}}
                        
                    <!-- /SIDEBAR -->


@endsection