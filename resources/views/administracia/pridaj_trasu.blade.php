@extends ('administracia.administracia')

@section ('administracia_content')

       

 
 <div class="col-lg-6 col-md-6 col-sm-12">
 	<h1>Pridať novú trasu</h1>
 	<br>
	 <form method="POST" action="/pridaj_trasu" enctype="multipart/form-data">
	    	{{csrf_field()}}
		<div class="row">
	    	<h3 class="block-title alt"><i class="fa fa-angle-down"></i>Názov a parametre trasy</h3>
			
	        <div class="form-group {{ $errors->has('nazov') ? 'has-error' : ''}}">
	            <input 
	            	name="nazov" 
	            	id="nazov" 
	            	data-toggle="tooltip"
	                class="form-control alt" 
	                type="text" 
	                placeholder="Názov trasy:"  
	                value="{{isset($post) ? old ('nazov', $post->nazov) :old('nazov')}}">
	            @if($errors->has('nazov'))
	            	<span 
	            		class="help-block">{{$errors->first('nazov')}}
	            	</span>
	            @endif
	        </div>

	        <div class="form-group {{ $errors->has('mapa') ? 'has-error' : ''}}">
	            <input 
	            	name="mapa" 
	            	id="mapa" 
	            	data-toggle="tooltip"
	                class="form-control alt" 
	                type="text" 
	                placeholder="Mapa trasy:"  
	                value="{{isset($post) ? old ('mapa', $post->mapa) :old('mapa')}}">
	            @if($errors->has('mapa'))
	            	<span 
	            		class="help-block">{{$errors->first('mapa')}}
	            	</span>
	            @endif
	        </div>

	        <div class="form-group {{ $errors->has('casova_narocnost') ? 'has-error' : ''}}">
	            <input 
	            	name="casova_narocnost" 
	            	id="casova_narocnost" 
	            	data-toggle="tooltip"
	                class="form-control alt" 
	                type="text" 
	                placeholder="Časová náročnosť trasy:"  
	                value="{{isset($post) ? old ('casova_narocnost', $post->casova_narocnost) :old('casova_narocnost')}}"
	                >
	            @if($errors->has('casova_narocnost'))
	            	<span 
	            		class="help-block">{{$errors->first('casova_narocnost')}}
	            	</span>
	            @endif
	        </div>

            <div class="form-group {{ $errors->has('dlzka') ? 'has-error' : ''}}">
                <input 
                    name="dlzka" 
                    id="dlzka" 
                    data-toggle="tooltip"
                    class="form-control alt" 
                    type="text" 
                    placeholder="Dĺžka trasy v km:"  
                    value="{{isset($post) ? old ('dlzka', $post->dlzka) :old('dlzka')}}"
                    >
                @if($errors->has('dlzka'))
                    <span 
                        class="help-block">{{$errors->first('dlzka')}}
                    </span>
                @endif
            </div>

	    </div>

	    

        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Fotogaléria trasy</h3>

        <div>
		   <input type="file" name="images[]" multiple>
	 	</div>

		<h3 class="block-title alt">
			<i class="fa fa-angle-down"></i>Pridať novú trasu 
		</h3>

		<div class="form-group">
			<button type="submit" class="btn btn-primary">Pridať</button>
		</div>
	</form>
</div>  
@endsection