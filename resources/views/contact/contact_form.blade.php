<form name="contact-form" method="post" action="#" class="contact-form alt" id="contact-form">

    <div class="row">
        <div class="col-md-6">

            <div class="outer required">
                <div class="form-group af-inner has-icon">
                    <label class="sr-only" for="name">Name</label>
                    <input
                            type="text" name="name" id="name" placeholder="Name" value="" size="30"
                            data-toggle="tooltip" title="Name is required"
                            class="form-control placeholder"/>
                    <span class="form-control-icon"><i class="fa fa-user"></i></span>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            <div class="outer required">
                <div class="form-group af-inner has-icon">
                    <label class="sr-only" for="email">Email</label>
                    <input
                            type="text" name="email" id="email" placeholder="Email" value="" size="30"
                            data-toggle="tooltip" title="Email is required"
                            class="form-control placeholder"/>
                    <span class="form-control-icon"><i class="fa fa-envelope"></i></span>
                </div>
            </div>

        </div>
    </div>

    <div class="form-group af-inner has-icon">
        <label class="sr-only" for="input-message">Message</label>
        <textarea
                name="message" id="input-message" placeholder="Message" rows="4" cols="50"
                data-toggle="tooltip" title="Message is required"
                class="form-control placeholder"></textarea>
        <span class="form-control-icon"><i class="fa fa-bars"></i></span>
    </div>

    <div class="outer required">
        <div class="form-group af-inner">
            <input type="submit" name="submit" class="form-button form-button-submit btn btn-block btn-theme"
                   id="submit_btn" value="Send message"/>
        </div>
    </div>

</form>