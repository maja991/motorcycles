<h2 class="section-title">
                    <small>Feel Free to Say Hello!</small>
                    <span>Get in Touch With Us</span>
                </h2>

                <div class="row">
                    <div class="col-md-6">
                        <!-- Contact form -->
                            @include ('contact.contact_form')
                        <!-- /Contact form -->
                    </div>
                    <div class="col-md-6">

                        

                        <ul class="media-list contact-list">
                            <li class="media">
                                <div class="media-left"><i class="fa fa-home"></i></div>
                                <div class="media-body">Adress: Motocuore s. r. o. Galvániho 12/A 821 04 Bratislava - mestská časť Ružinov
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa"></i></div>
                                
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-phone"></i></div>
                                <div class="media-body">Support Phone:  +421 948 281 234 / +421 948 288 826</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-envelope"></i></div>
                                <div class="media-body">E mails: info@motocuore.sk</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-clock-o"></i></div>
                                <div class="media-body">Working Hours: 00:00-00:00</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-map-marker"></i></div>
                                
                                <div class="media-body">
                                    <a href="https://goo.gl/maps/SZNKwMHnimQ2"> 
                                        View on The Map
                                    </a>
                                </div>

                            </li>
                        </ul>

                    </div>
                </div>
