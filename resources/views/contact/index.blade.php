@extends ('layouts.master')
@section ('content')
    <div class="content-area">

            <!-- BREADCRUMBS -->
            <section class="page-section breadcrumbs text-center">
                <div class="container">
                    <div class="page-header">
                        <h1>Kontakt</h1>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="/">Domov</a></li>
                        <li class="active">Kontakt</li>
                    </ul>
                </div>
            </section>
            <!-- /BREADCRUMBS -->

            <!-- PAGE -->
            <section class="page-section color">
                <div class="container">

                    <div class="row">

                        <div class="col-md-4">
                            <div class="contact-info">

                                <h2 class="block-title"><span>Kontakt</span></h2>

                                <div class="media-list">
                                    <div class="media">
                                        <i class="pull-left fa fa-home"></i>
                                        <div class="media-body">
                                            <strong>Adresa:</strong><br>
                                            Galvániho 12/A 821 04 Bratislava - mestská časť Ružinov
                                        </div>
                                    </div>
                                    <div class="media">
                                        <i class="pull-left fa fa-phone"></i>
                                        <div class="media-body">
                                            <strong>Telefón:</strong><br>
                                           +421 948 281 234 / +421 948 288 826
                                        </div>
                                    </div>
                                    <div class="media">
                                        <i class="pull-left fa fa-envelope-o"></i>
                                        <div class="media-body">
                                            <strong>Fax:</strong><br>
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="media">
                                        <div class="media-body">
                                            <strong>Zákaznícky servis:</strong><br>
                                            <a href="mailto:E-mail: info@motocuore.sk">info@motocuore.sk</a>
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                        </div>
<!--
                        <div class="col-md-8 text-left">

                            <h2 class="block-title"><span>Contact Form</span></h2>

                            <!-- Contact form --X>
                            <form name="contact-form" method="post" action="#" class="contact-form" id="contact-form">

                                <div class="outer required">
                                    <div class="form-group af-inner">
                                        <label class="sr-only" for="name">Name</label>
                                        <input
                                                type="text" name="name" id="name" placeholder="Name" value="" size="30"
                                                data-toggle="tooltip" title="Name is required"
                                                class="form-control placeholder"/>
                                    </div>
                                </div>

                                <div class="outer required">
                                    <div class="form-group af-inner">
                                        <label class="sr-only" for="email">Email</label>
                                        <input
                                                type="text" name="email" id="email" placeholder="Email" value="" size="30"
                                                data-toggle="tooltip" title="Email is required"
                                                class="form-control placeholder"/>
                                    </div>
                                </div>

                                <div class="outer required">
                                    <div class="form-group af-inner">
                                        <label class="sr-only" for="subject">Subject</label>
                                        <input
                                                type="text" name="subject" id="subject" placeholder="Subject" value="" size="30"
                                                data-toggle="tooltip" title="Subject is required"
                                                class="form-control placeholder"/>
                                    </div>
                                </div>

                                <div class="form-group af-inner">
                                    <label class="sr-only" for="input-message">Message</label>
                                    <textarea
                                            name="message" id="input-message" placeholder="Message" rows="4" cols="50"
                                            data-toggle="tooltip" title="Message is required"
                                            class="form-control placeholder"></textarea>
                                </div>

                                <div class="outer required">
                                    <div class="form-group af-inner">
                                        <input type="submit" name="submit" class="form-button form-button-submit btn btn-theme btn-theme-dark" id="submit_btn" value="Send message" />
                                    </div>
                                </div>

                            </form>
                            <!-- /Contact form --X>

                        </div>

                    -->

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2660.75549019838!2d17.171913815802334!3d48.17279355668608!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476c8f1f6e58664d%3A0x7aca754c83813587!2sGalvaniho+16605%2F12A%2C+821+04+Bratislava!5e0!3m2!1ssk!2ssk!4v1533183308939" width=100% height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

                    </div>

                </div>
            </section>
            <!-- /PAGE -->

            <!-- PAGE -->
            <section class="page-section no-padding">
                <div class="container full-width">

                    <!-- Google map -->
                    <div class="google-map">
                        <div id="map-canvas"></div>
                    </div>
                    <!-- /Google map -->

                </div>
            </section>
            <!-- /PAGE -->

        </div>
@endsection