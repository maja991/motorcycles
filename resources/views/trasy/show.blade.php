@extends ('layouts.master')

@section ('content')

        <!-- BREADCRUMBS -->
        <section class="page-section breadcrumbs text-right">
            <div class="container">
                <div class="page-header">
                    <h1>{{ $trasa->nazov }}</h1>
                </div>
                <ul class="breadcrumb">
                    <li><a href="/motorky">Home</a></li>
                    <li><a href="/trasy">Trasy</a></li>
                    <li class="active">{{ $trasa->nazov }}</li>
                </ul>
            </div>
        </section>
        <!-- /BREADCRUMBS -->

<div class="col-sm-8 blog-main">
    <a href="/trasy/{{$trasa->id}}">
            <h2 class="blog-post-title">
            	{{ $trasa->nazov }}
            </h2> 
    </a>

    <hr>
    
    	<table>

		  <tr>
		    <td>Dĺžka : </td>
		    <td>{{ $trasa->dlzka }}</td>
		  </tr>

		  <tr>
		    <td>Časová náročnosť : </td>
		    <td>{{ $trasa->casova_narocnost }}</td>
		  </tr>

		</table>

	<hr>

	@if ( ( $trasa->mapa )!="" )

	<h2 class="">
	    Mapa trasy
	</h2>
		
	<iframe src={{ $trasa->mapa }} width=100% height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

	<hr>

	@endif

	<h2 class="">
	    Fotogaléria
	</h2>


	<div class="container">
		<div class="gallery">

			@foreach ($trasa->images as $image)
				<a href="../storage/images-route/{{$image->nazov}}" >
					<img src="../storage/images-route/{{$image->nazov}}" alt="" title="{{$image->nazov}}">
				</a>
			@endforeach
	
		</div>
	</div>

	<form method="POST" action="/trasy/{{$trasa->id}}/pridaj_obrazok_trasy" enctype="multipart/form-data">
    	{{csrf_field()}}
    	<h3></i>Pridaj fotky</h3>
    	<div>
		   <input type="file" name="images[]" multiple>
	 	</div>

	 	<div class="form-group">
			<button type="submit" class="btn btn-primary">Pridaj fotky</button>
		</div>

	</form>

	<hr>

	<h2 class="">
	    Komentáre k trase
	</h2>

	<div class="comments">
		<ul class="list-group">
			@foreach ($trasa->comments as $comment)
			<li class="list-group-item">
				{{$comment->komentar}}
			</li>
			@endforeach
		</ul>
	</div>

	<hr>

	<div class="card">
		<div class="card-block">
			<form method="POST" action="/trasy/{{$trasa->id}}/komentare">
				{{csrf_field()}}
				<div class="form-group">
				<textarea name="komentar" placeholder="Komentár" 
						class="form-control" required></textarea>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">
						Pridaj komentár
					</button>
				</div>
			</form>
		</div>
	</div>
</div>





@endsection

