
@extends ('layouts.master')
@section ('content')
  
  <div class="col-sm-8 blog-main">
    <h2>Trasy</h2>
    <hr>
	    @foreach ($trasy as $trasa)
	    	@include ('trasy.trasa')
	    @endforeach
          
  </div>

@endsection

