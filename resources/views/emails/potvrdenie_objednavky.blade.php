@component('mail::message')
# Potvrdenie objednávky
<br>
Dobrý deň {{$objednavka['meno']}} {{$objednavka['priezvisko']}}.
<br>
Vaša objednávka motorky {{$objednavka['znacka']}} {{$objednavka['model']}} bola prijatá a už sa jej venujeme. 
<br>
<br>
Objednaná adresa pristavennia : {{$objednavka['adresa_pristavenia']}}.
<br>
Ďakujeme za Vašu objednávku.

@endcomponent
