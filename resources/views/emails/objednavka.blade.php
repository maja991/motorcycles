@component('mail::message')
# Nová Objednávka
<br>
Do systému bola prijatá nová objednávka
<br>
<table>
	<tr>
		<td>Meno : </td>
		<td>{{ $objednavka['meno'] }}</td>
	</tr>

	<tr>
		<td>Priezvisko : </td>
		<td>{{ $objednavka['priezvisko'] }}</td>
	</tr>

	<tr>
		<td>Email : </td>
		<td>{{ $objednavka['email'] }}</td>
	</tr>

	<tr>
		<td>Telefón : </td>
		<td>{{ $objednavka['telefon'] }}</td>
	</tr>

	<tr>
		<td>Adresa pristavenia : </td>
		<td>{{ $objednavka['adresa_pristavenia'] }}</td>
	</tr>

	<tr>
		<td>Adresa vrátenia : </td>
		<td>{{ $objednavka['adresa_vratenia'] }}</td>
	</tr>

	<tr>
		<td>Motorka : </td>
		<td>{{ $objednavka['id_motorky'] }}</td>
		<td>{{ $objednavka['znacka'] }} {{ $objednavka['model'] }}</td>
	</tr>

	<tr>
		<td>Príslušenstvo : </td>
		<td>{{ $objednavka['prislusenstvo'] }}</td>
	</tr>
</table>	
<br>
@endcomponent
