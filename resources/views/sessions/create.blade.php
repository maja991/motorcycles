@extends ('layouts.master')
@section ('content')
	<div class="col-sm-8">
		<h1>Prihlasovací formulár</h1>

		<form action="/login" method="POST">
			{{csrf_field()}}

			<div class="form-group">
				<label for="email">Email :</label>
				<input type="email" class="form-control" id="email" name="email" required>
			</div>

			<div class="form-group">
				<label for="password">Heslo :</label>
				<input type="password" class="form-control" id="password" name="password" required>
			</div>
	
			<div class="form-group">
				<button type="submit" class="btn-btn-primary">Prihlás sa</button>
			</div>

		</form>
		@include('layouts.errors')
	</div>
@endsection