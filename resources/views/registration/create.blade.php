@extends ('layouts.master')
@section ('content')
<div class="col-sm-3">
</div>	
	<div class="col-sm-6">
		<h1>Registračný formulár</h1>

		<form action="/register" method="POST">
			{{csrf_field()}}

			<h3 class="block-title alt">Informácie potrebné pre vytvorenie používateľského účtu <br> (všetky údaje sú povinné)</h3>

				<div class="form-group">
					<label for="name">Prihlasovacie meno :</label>
					<input 
						type="text" 
						class="form-control" 
						id="name" 
						name="name" 
						placeholder="Prihlasovacie meno" 
						required
					>
				</div>

				<div class="form-group">
					<label for="email">Email :</label>
					<input 
						type="text" 
						class="form-control" 
						id="email" 
						name="email"
						placeholder="Email"  
						required
					>
				</div>

				<div class="form-group">
					<label for="password">Heslo :</label>
					<input 
						type="password" 
						class="form-control" 
						id="password" 
						name="password" 
						placeholder="Heslo"
						required
					>
				</div>

				<div class="form-group">
					<label for="password_confirmation">Potvrdenie hesla :</label>
					<input 
						type="password" 
						class="form-control" 
						id="password_confirmation" 
						name="password_confirmation"
						placeholder="Zopakuj heslo" 
						required
					>
				</div>
	
				

			<h3 class="block-title alt">Informácie o zákazníkovi potrebné pre objednávky <br> (údaje sú nepovinné)</h3>

			<div class="form-group">
				<label>Pohlavie :</label>
				<br>
	            <div class="radio radio-inline">
	                <input type="radio" id="inlineRadio1" value="Pán" name="gender_radioInline" checked="">
	                <label for="inlineRadio1">Pán</label>
	            </div>
	            <div class="radio radio-inline">
	                <input type="radio" id="inlineRadio2" value="Pani" name="gender_radioInline">
	                <label for="inlineRadio2">Pani</label>
	            </div>
	        </div>

			<div class="form-group">
				<label for="meno">Meno :</label>
				<input 
					name="meno" 
                	id="meno" 
                	data-toggle="tooltip"
                    class="form-control" 
                    type="text" 
                    placeholder="Meno"  
                    value="{{isset($post) ? old ('meno', $post->meno) :old('meno')}}"
					>
			</div>

			<div class="form-group">
				<label for="priezvisko">Priezvisko :</label>
				<input 
					name="priezvisko" 
                	id="priezvisko" 
                	data-toggle="tooltip"
                    class="form-control" 
                    type="text" 
                    placeholder="Peiezvisko"  
                    value="{{isset($post) ? old ('priezvisko', $post->priezvisko) :old('priezvisko')}}"
					>
			</div>

			<div class="form-group">
				<label for="telefon">Telefón :</label>
				<input 
					name="telefon" 
                	id="telefon" 
                	data-toggle="tooltip"
                    class="form-control" 
                    type="text" 
                    placeholder="Telefón"  
                    value="{{isset($post) ? old ('telefon', $post->telefon) :old('telefon')}}"
					>
			</div>

			<div class="form-group">
				<button type="submit" class="btn-btn-primary">Zaregistruj sa</button>
			</div>

		</form>
		@include('layouts.errors')
	</div>


@endsection