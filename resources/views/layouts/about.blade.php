<div class="widget">
	<h4 class="widget-title">About Us</h4>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sollicitudin ultrices suscipit. Sed commodo vel mauris vel dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
	<ul class="social-icons">
	    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
	    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
	    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
	    <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
	</ul>
</div>