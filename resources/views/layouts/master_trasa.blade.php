<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Motocuore</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../theme/ico/apple-touch-icon-144-precomposed.png">
    <link rel="shortcut icon" href="../theme/ico/favicon.ico">

    <!-- CSS Global -->
    <link href="../theme/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        
    <link href="../theme/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="
    stylesheet">
       
    <link href="../theme/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        
    <link href="../theme/plugins/prettyphoto/css/prettyPhoto.css" rel="stylesheet">
        
    <link href="../theme/plugins/owl-carousel2/assets/owl.carousel.min.css" rel="stylesheet">
       
    <link href="../theme/plugins/owl-carousel2/assets/owl.theme.default.min.css" rel="stylesheet">
        
    <link href="../theme/plugins/animate/animate.min.css" rel="stylesheet">
       
    <link href="../theme/plugins/swiper/css/swiper.min.css" rel="stylesheet">
        
    <link href="../theme/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
       
    <link href="../theme/plugins/countdown/jquery.countdown.css" rel="stylesheet">
       
    <link href="../theme/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        

    <!-- Theme CSS -->
    <link href="../theme/css/theme.css" rel="stylesheet">   

    <!-- Head Libs -->
    <script src="../theme/plugins/modernizr.custom.js"></script>
      

    <!--[if lt IE 9]>
    <script src="../theme/plugins/iesupport/html5shiv.js"></script>
    <script src="../theme/plugins/iesupport/respond.min.js"></script>
    <![endif]-->

    <!-- simplelightbox - trasy -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Simple Lightbox - Responsive touch friendly Image lightbox</title>
    <link href='http://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
    <link href='../simplelightbox/simplelightbox.min.css' rel='stylesheet'>
    <link href='../simplelightbox/demo.css' rel='stylesheet/scss'>
    <link href='../simplelightbox/gallery.css' rel='stylesheet'>
    <!-- simplelightbox - trasy -->

</head>
<body id="home" class="wide">
<!-- PRELOADER -->
<div id="preloader">
    <div id="preloader-status">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
        <div id="preloader-title">Loading</div>
    </div>
</div>
<!-- /PRELOADER -->

<!-- WRAPPER -->
<div class="wrapper">

    <!-- HEADER -->
    <header class="header fixed">
        <div class="header-wrapper">
            <div class="container">

                <!-- Logo -->
                <div class="logo">
                    <a href="index.html"><img src="../theme/img/logo-rentit.png" alt="Rent It"/></a>
                </div>
                <!-- /Logo -->

                <!-- Mobile menu toggle button -->
                <a href="#" class="menu-toggle btn btn-theme-transparent"><i class="fa fa-bars"></i></a>
                <!-- /Mobile menu toggle button -->

                <!-- Navigation -->
                    @include ('layouts.nav')
                <!-- /Navigation -->

            </div>
        </div>

    </header>
    <!-- /HEADER -->


    <!-- CONTENT AREA -->
        @yield ('content')
    <!-- /CONTENT AREA -->

    <!-- FOOTER -->
        @include ('layouts.footer')
    <!-- /FOOTER -->

    <div id="to-top" class="to-top"><i class="fa fa-angle-up"></i></div>

</div>
<!-- /WRAPPER -->

<!-- JS Global -->
<script src="../theme/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="../theme/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="../theme/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<script src="../theme/plugins/superfish/js/superfish.min.js"></script>
<script src="../theme/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script>
<script src="../theme/plugins/owl-carousel2/owl.carousel.min.js"></script>
<script src="../theme/plugins/jquery.sticky.min.js"></script>
<script src="../theme/plugins/jquery.easing.min.js"></script>
<script src="../theme/plugins/jquery.smoothscroll.min.js"></script>
<!--<script src="../theme/plugins/smooth-scrollbar.min.js"></script>-->
<script src="../theme/plugins/swiper/js/swiper.jquery.min.js"></script>
<script src="../theme/plugins/datetimepicker/js/moment-with-locales.min.js"></script>
<script src="../theme/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<!-- JS Page Level -->
<script src="../theme/js/theme-ajax-mail.js"></script>
<script src="../theme/js/theme.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="../simplelightbox/simple-lightbox.js"></script>
<script>
    $(function(){
        var $gallery = $('.gallery a').simpleLightbox();

        $gallery.on('show.simplelightbox', function(){
            console.log('Requested for showing');
        })
        .on('shown.simplelightbox', function(){
            console.log('Shown');
        })
        .on('close.simplelightbox', function(){
            console.log('Requested for closing');
        })
        .on('closed.simplelightbox', function(){
            console.log('Closed');
        })
        .on('change.simplelightbox', function(){
            console.log('Requested for change');
        })
        .on('next.simplelightbox', function(){
            console.log('Requested for next');
        })
        .on('prev.simplelightbox', function(){
            console.log('Requested for prev');
        })
        .on('nextImageLoaded.simplelightbox', function(){
            console.log('Next image loaded');
        })
        .on('prevImageLoaded.simplelightbox', function(){
            console.log('Prev image loaded');
        })
        .on('changed.simplelightbox', function(){
            console.log('Image changed');
        })
        .on('nextDone.simplelightbox', function(){
            console.log('Image changed to next');
        })
        .on('prevDone.simplelightbox', function(){
            console.log('Image changed to prev');
        })
        .on('error.simplelightbox', function(e){
            console.log('No image found, go to the next/prev');
            console.log(e);
        });
    });
</script>


</body>
</html>