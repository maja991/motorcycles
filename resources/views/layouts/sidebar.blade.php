<aside class="col-md-3 sidebar" id="sidebar">
    <!-- widget helping center -->
                        <div class="widget shadow widget-helping-center">
                            <h4 class="widget-title">Kontakt</h4>
                            <div class="widget-content">
                                <p>Ak máte otázky neváhajte nás kontaktovať.</p>
                                <h5 class="widget-title-sub">+421 948 281 234 / +421 948 288 826</h5>
                                <p><a href="mailto:support@supportcenter.com">info@motocuore.sk</a></p>
                                <div class="button">
                                    <a href="/kontakt" class="btn btn-block btn-theme btn-theme-dark">Kontaktujte nás</a>
                                </div>
                            </div>
                        </div>
                        <!-- /widget helping center -->
                        <!-- widget -->

                        <!-- price filter -->
                        <div class="widget shadow widget-find-car">
                            <h4 class="widget-title">Filtrovanie podľa ceny</h4>
                            <div class="widget-content">
                                <!-- Search form -->
                                <div class="form-search light">
                                    <form method="GET" action="/motorky">
                                        <div class="form-group has-icon has-label">

                                                <input 
                                                type="text" 
                                                name="min_price" 
                                                placeholder="Najnižšia cena"
                                                value="{{request()->min_price}}"
                                                class="form-control"
                                                >
                                            <input 
                                                type="text" 
                                                name="max_price" 
                                                placeholder="Najvyššia cena"
                                                value="{{request()->max_price}}"
                                                class="form-control"
                                                >
                                            <br>
                                                <button type="submit" class="btn btn-submit btn-theme btn-theme-dark btn-block">Hľadať</button>
                                        </div>
                                    </form>

                                    

                                        

                                   
                                </div>
                                <!-- /Search form -->
                            </div>
                        </div>
                        <div>
                            
                        </div>
                        
                        <!-- /price filter -->
<!--
                        <div class="widget shadow widget-find-car">
                            <h4 class="widget-title">Find Best Rental Car</h4>
                            <div class="widget-content">
                                <!-- Search form -- l>
                                <div class="form-search light">
                                    <form action="#">

                                        <div class="form-group has-icon has-label">
                                            <label for="formSearchUpLocation3">Picking Up Location</label>
                                            <input type="text" class="form-control" id="formSearchUpLocation3" placeholder="Airport or Anywhere">
                                            <span class="form-control-icon"><i class="fa fa-map-marker"></i></span>
                                        </div>

                                        <div class="form-group has-icon has-label">
                                            <label for="formSearchOffLocation3">Dropping Off Location</label>
                                            <input type="text" class="form-control" id="formSearchOffLocation3" placeholder="Airport or Anywhere">
                                            <span class="form-control-icon"><i class="fa fa-map-marker"></i></span>
                                        </div>

                                        <div class="form-group has-icon has-label">
                                            <label for="formSearchUpDate3">Picking Up Date</label>
                                            <input type="text" class="form-control" id="formSearchUpDate3" placeholder="dd/mm/yyyy">
                                            <span class="form-control-icon"><i class="fa fa-calendar"></i></span>
                                        </div>

                                        <div class="form-group has-icon has-label selectpicker-wrapper">
                                            <label>Picking Up Hour</label>
                                            <select
                                                    class="selectpicker input-price" data-live-search="true" data-width="100%"
                                                    data-toggle="tooltip" title="Select">
                                                <option>20:00 AM</option>
                                                <option>21:00 AM</option>
                                                <option>22:00 AM</option>
                                            </select>
                                            <span class="form-control-icon"><i class="fa fa-clock-o"></i></span>
                                        </div>

                                        <div class="form-group selectpicker-wrapper">
                                            <label>Select Category</label>
                                            <select
                                                    class="selectpicker input-price" data-live-search="true" data-width="100%"
                                                    data-toggle="tooltip" title="Select">
                                                <option>Select Category</option>
                                                <option>Select Category</option>
                                                <option>Select Category</option>
                                            </select>
                                        </div>

                                        <div class="form-group selectpicker-wrapper">
                                            <label>Select Type</label>
                                            <select
                                                    class="selectpicker input-price" data-live-search="true" data-width="100%"
                                                    data-toggle="tooltip" title="Select">
                                                <option>Select Type</option>
                                                <option>Select Type</option>
                                                <option>Select Type</option>
                                            </select>
                                        </div>

                                        <button type="submit" id="formSearchSubmit3" class="btn btn-submit btn-theme btn-theme-dark btn-block">Find Car</button>

                                    </form>
                                </div>
                                <!-- /Search form --k>
                            </div>
                        </div> -->
                        <!-- /widget -->
                        
                        <!-- widget testimonials -->
                        <!--
                        <div class="widget shadow">
                            <div class="widget-title">Testimonials</div>
                            <div class="testimonials-carousel">
                                <div class="owl-carousel" id="testimonials">
                                    <div class="testimonial">
                                        <div class="media">
                                            <div class="media-body">
                                                <div class="testimonial-text">Vivamus eget nibh. Etiam cursus leo vel metus. Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.</div>
                                                <div class="testimonial-name">John Doe <span class="testimonial-position">Co- founder at Rent It</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="testimonial">
                                        <div class="media">
                                            <div class="media-body">
                                                <div class="testimonial-text">Vivamus eget nibh. Etiam cursus leo vel metus. Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.</div>
                                                <div class="testimonial-name">John Doe <span class="testimonial-position">Co- founder at Rent It</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="testimonial">
                                        <div class="media">
                                            <div class="media-body">
                                                <div class="testimonial-text">Vivamus eget nibh. Etiam cursus leo vel metus. Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.</div>
                                                <div class="testimonial-name">John Doe <span class="testimonial-position">Co- founder at Rent It</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        -->
                        <!-- /widget testimonials -->
                        
                    </aside>