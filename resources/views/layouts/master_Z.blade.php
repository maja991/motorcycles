<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Motocuore</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/blog/">

        <!-- Custom styles for this template -->
    <link href="{{asset('css/app.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{asset('css/style.css') }}" rel="stylesheet" type="text/css" >


    <!--  <link href="/motocuore/public/css/app.css" rel="stylesheet"> -->
  </head>

  <body>

   
   @if ($flash = session('message'))
     <div id="flash-message" class="alert alert-success" role="alert">
      {{$flash}}
     </div>
   @endif
    <div class="blog-header">
      <div class="container">
        <h1 class="blog-title">Motocuore</h1>
      </div>
    </div>
    <div class="container">
      <div class="col-sm-8">
        @include ('layouts.nav')
      </div>

      <div class="row">

        @yield ('content')
        
      </div><!-- /.row -->

    </div><!-- /.container -->
    @include ('layouts.footer')


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>