
<div class="row">
    <div class="col-md-12">
		<h3>Objednávkový formulár</h3>

		<form method="POST" action="/objednaj">

			{{csrf_field()}}

			<div class="form-group">
	    		<select name="motorky" class="selectpicker">
					@foreach ($motorky as $motorka)
		    			<option value= {{$motorka->id}}>
		    				{{$motorka->znacka}} {{$motorka->model}}
		    			</option>
		    		@endforeach
				</select>
			</div>
		    

		    <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Informácie o zákazníkovi</h3>

		    <div class="row">
		        <div class="col-md-12">
		            <div class="radio radio-inline">
		                <input type="radio" id="inlineRadio1" value="Pán" name="radioInline" checked="">
		                <label for="inlineRadio1">Pán</label>
		            </div>
		            <div class="radio radio-inline">
		                <input type="radio" id="inlineRadio2" value="Pani" name="radioInline">
		                <label for="inlineRadio2">Pani</label>
		            </div>
		        </div>
		        <div class="col-md-6">
		            <div class="form-group {{ $errors->has('meno') ? 'has-error' : ''}}">
		                <input 
		                	name="meno" 
		                	id="meno" 
		                	data-toggle="tooltip"
		                    class="form-control alt" 
		                    type="text" 
		                    placeholder="Meno: *"  
		                    value="{{isset($post) ? old ('meno', $post->meno) :old('meno')}}">
	                    @if($errors->has('meno'))
	                    	<span 
	                    		class="help-block">{{$errors->first('meno')}}
	                    	</span>
	                    @endif
		            </div>
                    <div class="form-group {{ $errors->has('priezvisko') ? 'has-error' : ''}}">
		                <input 
		                	name="priezvisko" 
		                	id="priezvisko" 
		                	data-toggle="tooltip"
		                	class="form-control alt" 
		                	type="text" 
		                	placeholder="Priezvisko: *"
		                	value="{{isset($post) ? old ('priezvisko', $post->meno) :old('priezvisko')}}">
	                    @if($errors->has('priezvisko'))
	                    	<span 
	                    		class="help-block">{{$errors->first('priezvisko')}}
	                    	</span>
	                    @endif
		            </div>
		        </div>

		        <div class="col-md-6">
		            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
		                <input 
		                	name="email" 
		                	id="email" 
		                	data-toggle="tooltip"
		                    class="form-control alt" 
		                    type="text" 
		                    placeholder="Email: *"
		                    value="{{isset($post) ? old ('email', $post->meno) :old('email')}}">
	                    @if($errors->has('email'))
	                    	<span 
	                    		class="help-block">{{$errors->first('email')}}
	                    	</span>
	                    @endif
                    </div>
                    <div class="form-group {{ $errors->has('telefon') ? 'has-error' : ''}}">
		            	<input 
		            		name="telefon" 
		            		id="telefon" 
		            		class="form-control alt" 
		            		type="text" 
		            		placeholder="Telefón:"
		            		value="{{isset($post) ? old ('telefon', $post->meno) :old('telefon')}}">
	                    @if($errors->has('telefon'))
	                    	<span 
	                    		class="help-block">{{$errors->first('telefon')}}
	                    	</span>
	                    @endif	
		            </div>
		        </div>
		        
		    </div>

		    <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Miesto pristavenia a vrátenia motorky </h3>

			<div class="row">
			    <div class="col-md-6">
			        <div class="form-group {{ $errors->has('miesto_pristavenia') ? 'has-error' : ''}}"">
			            <input 
			            	name="miesto_pristavenia" 
			            	id="miesto_pristavenia" 
			            	data-toggle="tooltip" 
			            	class="form-control alt" 
			            	type="text" 
			            	placeholder="Adresa pristavenia: *"
			            	value="{{isset($post) ? old ('miesto_pristavenia', $post->meno) :old('miesto_pristavenia')}}">
	                    @if($errors->has('miesto_pristavenia'))
	                    	<span 
	                    		class="help-block">{{$errors->first('miesto_pristavenia')}}
	                    	</span>
	                    @endif
	            	</div>
	            	<div class="form-group {{ $errors->has('miesto_vratenia') ? 'has-error' : ''}}"">
			            <input 
			            	name="miesto_vratenia" 
			            	id="miesto_vratenia" 
			            	data-toggle="tooltip" 
			            	class="form-control alt" 
			            	type="text" 
			            	placeholder="Adresa vrátenia: *"
			            	value="{{isset($post) ? old ('miesto_vratenia', $post->meno) :old('miesto_vratenia')}}">
	                    @if($errors->has('miesto_vratenia'))
	                    	<span 
	                    		class="help-block">{{$errors->first('miesto_vratenia')}}
	                    	</span>
	                    @endif
		            </div>
		        </div>
		    </div>

			<h3 class="block-title alt"><i class="fa fa-angle-down"></i>Príslušenstvo</h3>

		    <div class="row">
		        <div class="col-md-6">
		            <div class="left">
		                <div class="checkbox checkbox-danger">
		                    <input id="prislusenstvo1" type="checkbox" value=1 name='prislusenstvo1'>
		                    <label for="prislusenstvo1">Príslušenstvo 1 <span class="pull-right">
		                    	
		                    </span></label>
		                </div>
		                <div class="checkbox checkbox-danger">
		                    <input id="prislusenstvo2" type="checkbox" value=1 name='prislusenstvo2'>
		                    <label for="prislusenstvo2">Príslušenstvo 2 <span class="pull-right">

		                    </span></label>
		                </div>
		                <div class="checkbox checkbox-danger">
		                    <input id="prislusenstvo3" type="checkbox" value=1 name='prislusenstvo3'>
		                    <label for="prislusenstvo">Príslušenstvo 3 <span class="pull-right">

		                    </span></label>
		                </div>
		                <div class="checkbox checkbox-danger">
		                    <input id="prislusenstvo4" type="checkbox" value=1 name='prislusenstvo4'>
		                    <label for="prislusenstvo4">Príslušenstvo 4 <span class="pull-right">

		                    </span></label>
		                </div>
		            </div>
		        </div>

		        <div class="col-md-6">
		            <div class="right">
		                <div class="checkbox checkbox-danger">
		                    <input id="prislusenstvo5" type="checkbox" value=1 name='prislusenstvo5'>
		                    <label for="prislusenstvo5">Príslušenstvo 5 <span class="pull-right">

		                    </span></label>
		                </div>
		                <div class="checkbox checkbox-danger">
		                    <input id="prislusenstvo6" type="checkbox" value=1 name='prislusenstvo6'>
		                    <label for="prislusenstvo6">Príslušenstvo 6 <span class="pull-right">

		                    </span></label>
		                </div>
		                <div class="checkbox checkbox-danger">
		                    <input id="prislusenstvo7" type="checkbox" value=1 name='prislusenstvo7'>
		                    <label for="prislusenstvo7">Príslušenstvo 7 <span class="pull-right">

		                    </span></label>
		                </div>
		                <div class="checkbox checkbox-danger">
		                    <input id="prislusenstvo8" type="checkbox" value=1 name='prislusenstvo8'>
		                    <label for="prislusenstvo8">Príslušenstvo 8 <span class="pull-right">

		                    </span></label>
		                </div>
		            </div>
		        </div>

		    </div>
		    <!-- 

			<h3 class="block-title alt"><i class="fa fa-angle-down"></i>Možnosti platby</h3>

		    <div class="panel-group payments-options" id="accordion" role="tablist" aria-multiselectable="true">
		        <div class="panel radio panel-default">
		            <div class="panel-heading" role="tab" id="headingOne">
		                <h4 class="panel-title">
		                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">
		                        <span class="dot"></span> Direct Bank Transfer
		                    </a>
		                </h4>
		            </div>
		            <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
		                <div class="panel-body">
		                    <div class="alert alert-success" role="alert">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sollicitudin ultrices suscipit. Sed commodo vel mauris vel dapibus.</div>
		                </div>
		            </div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading" role="tab" id="headingTwo">
		                <h4 class="panel-title">
		                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapseTwo">
		                        <span class="dot"></span> Cheque Payment
		                    </a>
		                </h4>
		            </div>
		            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
		                <div class="panel-body">
		                    Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.
		                </div>
		            </div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading" role="tab" id="headingThree">
		                <h4 class="panel-title">
		                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapseThree">
		                        <span class="dot"></span> Credit Card
		                    </a>
		            <span class="overflowed pull-right">
		                <img src="assets/img/preview/payments/mastercard-2.jpg" alt=""/>
		                <img src="assets/img/preview/payments/visa-2.jpg" alt=""/>
		                <img src="assets/img/preview/payments/american-express-2.jpg" alt=""/>
		                <img src="assets/img/preview/payments/discovery-2.jpg" alt=""/>
		                <img src="assets/img/preview/payments/eheck-2.jpg" alt=""/>
		            </span>
		                </h4>
		            </div>
		            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3"></div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading" role="tab" id="heading4">
		                <h4 class="panel-title">
		                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
		                        <span class="dot"></span> PayPal
		                    </a>
		                    <span class="overflowed pull-right"><img src="assets/img/preview/payments/paypal-2.jpg" alt=""/></span>
		                </h4>
		            </div>
		            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4"></div>
		        </div>
		    </div>
			-->
			<h3 class="block-title alt"><i class="fa fa-angle-down"></i>Odošli objednávku </h3>

			

  			<div class="form-group">
				<button type="submit" class="btn btn-primary">Objednaj</button>
			</div>
    	</form>
	</div>
</div>


