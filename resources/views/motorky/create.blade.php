@extends ('administracia.administracia')

@section ('administracia_content')

        <!-- BREADCRUMBS -->
        <section class="page-section breadcrumbs text-right">
            <div class="container">
                <div class="page-header">
                    <h1>Administrácia</h1>
                </div>
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="/administracia">Administrácia</a></li>
                    <li class="active">Pridať motorku</li>
                </ul>
            </div>
        </section>
        <!-- /BREADCRUMBS -->

 
 <div class="col-lg-6 col-md-6 col-sm-12">
 	<h1>Pridať novú motorku</h1>
 	<br>
	 <form method="POST" action="/pridaj_motorku" enctype="multipart/form-data">
	    	{{csrf_field()}}
		<div class="row">
	    	<h3 class="block-title alt"><i class="fa fa-angle-down"></i>Značka, model</h3>
			
	        <div class="form-group {{ $errors->has('znacka') ? 'has-error' : ''}}">
	            <input 
	            	name="znacka" 
	            	id="znacka" 
	            	data-toggle="tooltip"
	                class="form-control alt" 
	                type="text" 
	                placeholder="Značka:"  
	                value="{{isset($post) ? old ('znacka', $post->znacka) :old('znacka')}}">
	            @if($errors->has('znacka'))
	            	<span 
	            		class="help-block">{{$errors->first('znacka')}}
	            	</span>
	            @endif
	        </div>

	        <div class="form-group {{ $errors->has('model') ? 'has-error' : ''}}">
	            <input 
	            	name="model" 
	            	id="model" 
	            	data-toggle="tooltip"
	                class="form-control alt" 
	                type="text" 
	                placeholder="Model:"  
	                value="{{isset($post) ? old ('model', $post->model) :old('model')}}">
	            @if($errors->has('model'))
	            	<span 
	            		class="help-block">{{$errors->first('model')}}
	            	</span>
	            @endif
	        </div>

	        <div class="form-group {{ $errors->has('popis') ? 'has-error' : ''}}">
	            <textarea 
	            	name="popis" 
	            	id="popis" 
	            	data-toggle="tooltip"
	                class="form-control alt" 
	                type="text" 
	                placeholder="Popis:"  
	                value="{{isset($post) ? old ('popis', $post->popis) :old('popis')}}"
	                rows="5">
                </textarea>
	            @if($errors->has('popis'))
	            	<span 
	            		class="help-block">{{$errors->first('popis')}}
	            	</span>
	            @endif
	        </div>

	        <div class="form-group">
	    		<select name="typ">
	    			<option value='Športovo cestovná'>
	    				Športovo cestovná
	    			</option>
	    			<option value='Superšport'>
	    				Superšport
	    			</option>
				</select>
			</div>

	    </div>

	    <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Parametre motorky</h3>

	    <div class="form-group {{ $errors->has('hmotnost') ? 'has-error' : ''}}">
            <input 
            	name="hmotnost" 
            	id="hmotnost" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Hmotnosť:"  
                value="{{isset($post) ? old ('hmotnost', $post->hmotnost) :old('hmotnost')}}">
            @if($errors->has('hmotnost'))
            	<span 
            		class="help-block">{{$errors->first('hmotnost')}}
            	</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('typ_motora') ? 'has-error' : ''}}">
			<input 
            	name="vykon" 
            	id="vykon" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Výkon :"  
                value="{{isset($post) ? old ('vykon', $post->vykon) :old('vykon')}}">
            @if($errors->has('vykon'))
            	<span 
            		class="help-block">{{$errors->first('vykon')}}
            	</span>
            @endif
        </div>
	  	<div class="form-group {{ $errors->has('typ_motora') ? 'has-error' : ''}}">
            <input 
            	name="typ_motora" 
            	id="typ_motora" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Typ motora:"  
                value="{{isset($post) ? old ('typ_motora', $post->typ_motora) :old('typ_motora')}}">
            @if($errors->has('typ_motora'))
            	<span 
            		class="help-block">{{$errors->first('typ_motora')}}
            	</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('objem_motora') ? 'has-error' : ''}}">
            <input 
            	name="objem_motora" 
            	id="objem_motora" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Objem motora:"  
                value="{{isset($post) ? old ('objem_motora', $post->objem_motora) :old('objem_motora')}}">
            @if($errors->has('objem_motora'))
            	<span 
            		class="help-block">{{$errors->first('objem_motora')}}
            	</span>
            @endif
        </div>

		<div class="form-group {{ $errors->has('prevodovka') ? 'has-error' : ''}}">
            <input 
            	name="prevodovka" 
            	id="prevodovka" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Prevodovka:"  
                value="{{isset($post) ? old ('prevodovka', $post->prevodovka) :old('prevodovka')}}">
            @if($errors->has('prevodovka'))
            	<span 
            		class="help-block">{{$errors->first('prevodovka')}}
            	</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('objem_nadrze') ? 'has-error' : ''}}">
            <input 
            	name="objem_nadrze" 
            	id="objem_nadrze" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Objem nádrže:"  
                value="{{isset($post) ? old ('objem_nadrze', $post->objem_nadrze) :old('objem_nadrze')}}">
            @if($errors->has('objem_nadrze'))
            	<span 
            		class="help-block">{{$errors->first('objem_nadrze')}}
            	</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('max_rychlost') ? 'has-error' : ''}}">
            <input 
            	name="max_rychlost" 
            	id="max_rychlost" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Maximálna rýchlosť:"  
                value="{{isset($post) ? old ('max_rychlost', $post->max_rychlost) :old('max_rychlost')}}">
            @if($errors->has('max_rychlost'))
            	<span 
            		class="help-block">{{$errors->first('max_rychlost')}}
            	</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('dojazd') ? 'has-error' : ''}}">
            <input 
            	name="dojazd" 
            	id="dojazd" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Dojazd:"  
                value="{{isset($post) ? old ('dojazd', $post->dojazd) :old('dojazd')}}">
            @if($errors->has('dojazd'))
            	<span 
            		class="help-block">{{$errors->first('dojazd')}}
            	</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('vyska_sedadla') ? 'has-error' : ''}}">
            <input 
            	name="vyska_sedadla" 
            	id="vyska_sedadla" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Výška sedadla:"  
                value="{{isset($post) ? old ('vyska_sedadla', $post->vyska_sedadla) :old('vyska_sedadla')}}">
            @if($errors->has('vyska_sedadla'))
            	<span 
            		class="help-block">{{$errors->first('vyska_sedadla')}}
            	</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('vodicske_opravnenie') ? 'has-error' : ''}}">
            <input 
            	name="vodicske_opravnenie" 
            	id="vodicske_opravnenie" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Vodičské oreávnenie :"  
                value="{{isset($post) ? old ('vodicske_opravnenie', $post->vodicske_opravnenie) :old('vodicske_opravnenie')}}">
            @if($errors->has('vodicske_opravnenie'))
            	<span 
            		class="help-block">{{$errors->first('vodicske_opravnenie')}}
            	</span>
            @endif
        </div>

        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Poplatky</h3>

        <div class="form-group {{ $errors->has('cennik') ? 'has-error' : ''}}">
            <textarea 
            	name="cennik" 
            	id="cennik" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Cenník:"  
                value="{{isset($post) ? old ('cennik', $post->cennik) :old('cennik')}}"
                rows="5"></textarea>
            @if($errors->has('cennik'))
            	<span 
            		class="help-block">{{$errors->first('cennik')}}
            	</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('kaucia') ? 'has-error' : ''}}">
            <input 
            	name="kaucia" 
            	id="kaucia" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Kaucia:"  
                value="{{isset($post) ? old ('kaucia', $post->kaucia) :old('kaucia')}}">
            @if($errors->has('kaucia'))
            	<span 
            		class="help-block">{{$errors->first('kaucia')}}
            	</span>
            @endif
        </div>

        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Poznámky</h3>

        <div class="form-group {{ $errors->has('cennik') ? 'has-error' : ''}}">
            <textarea 
            	name="poznamka" 
            	id="poznamka" 
            	data-toggle="tooltip"
                class="form-control alt" 
                type="text" 
                placeholder="Poznámky :"  
                value="{{isset($post) ? old ('poznamka', $post->poznamka) :old('poznamka')}}"
                rows="8"></textarea>
            @if($errors->has('poznamka'))
            	<span 
            		class="help-block">{{$errors->first('poznamka')}}
            	</span>
            @endif
        </div>

        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Fotogaléria motorky</h3>

        <div>
		   <input type="file" name="images[]" multiple>
	 	</div>

		<h3 class="block-title alt">
			<i class="fa fa-angle-down"></i>Pridať novú motorku 
		</h3>

		<div class="form-group">
			<button type="submit" class="btn btn-primary">Pridať</button>
		</div>
	</form>
</div>  
@endsection