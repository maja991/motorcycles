@extends ('layouts.master')
@section ('content')
  
  <div class="content-area">

        <!-- BREADCRUMBS -->
        <section class="page-section breadcrumbs text-right">
            <div class="container">
                <div class="page-header">
                    <h1>Motorky</h1>
                </div>
                <ul class="breadcrumb">
                    <li><a href="/motorky">Domov</a></li>
                    <li class="active">Motorky</li>
                </ul>
            </div>
        </section>
        <!-- /BREADCRUMBS -->

        <!-- PAGE WITH SIDEBAR -->
        <section class="page-section with-sidebar sub-page">
            <div class="container">
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-md-9 content car-listing" id="content">

 <!-- Zoznam motoriek -->
<!-- 
        {{request()->min_price}}
        {{request()->max_price}}
-->
      @if (isset (request()->max_price) && isset (request()->min_price))
          @foreach ($motorky as $motorka)
            @if ($motorka->kaucia <= request()->max_price && $motorka->kaucia >= request()->min_price)
                @include ('motorky.motorka')
            @endif
          @endforeach
      @elseif (isset (request()->max_price))
        @foreach ($motorky as $motorka)
            @if ($motorka->kaucia <= request()->max_price)
                @include ('motorky.motorka')
            @endif
          @endforeach
      @elseif (isset (request()->min_price)) 
        @foreach ($motorky as $motorka)
            @if ($motorka->kaucia >= request()->min_price)
                @include ('motorky.motorka')
            @endif
          @endforeach
      @else   
        @foreach ($motorky as $motorka)
            @include ('motorky.motorka')
        @endforeach
      @endif

 <!-- / Zoznam motoriek -->
                    

                        <!-- Pagination -->
                        <!-- <div class="pagination-wrapper">
                            <ul class="pagination">
                                <li class="disabled"><a href="#"><i class="fa fa-angle-double-left"></i> Previous</a></li>
                                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">Next <i class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </div> -->
                        <!-- /Pagination -->

                    </div>
                    <!-- /CONTENT -->

                    <!-- SIDEBAR -->
                    @include ('layouts.sidebar')
                        
                    <!-- /SIDEBAR -->

                </div>
            </div>
        </section>
        <!-- /PAGE WITH SIDEBAR -->

        <!-- PAGE -->{{--
        <section class="page-section contact dark">
            <div class="container">

                <!-- Get in touch -->

                    @include ('contact.get_in_touch')

                <!-- /Get in touch -->

            </div>
        </section>--}}
        <!-- /PAGE -->

    </div>


@endsection

