<a href="/motorky/{{$motorka->id}}">
  <div class="thumbnail no-border no-padding thumbnail-car-card clearfix">
    <div class="media" style="width:60%">

        @if(count($motorka->images)!=0) 
        <img 
        src="../storage/images/{{$motorka->images[0]->nazov}}"
        alt="{{$motorka->images[0]->nazov}}"
        style="max-height:40vh"
        >
        @endif

    
    </div>
    <div class="caption">
      <!--<div class="rating">
      <span class="star"></span>
              <span class="star active"></span>
              <span class="star active"></span>
              <span class="star active"></span>
            <span class="star active"></span>
          </div> -->
          <h4 class="caption-title">
            {{ $motorka->znacka }} {{ $motorka->model }}

          </h4>

          <table>
          	<tr>
              <td>popis : </td>
              <td>{{ $motorka->popis }}</td>
            </tr>

            <tr>
              <td>typ : </td>
              <td>{{ $motorka->typ }}</td>
            </tr>

            <tr>
              <td>hmotnosť : </td>
              <td>{{ $motorka->hmotnost }}</td>
            </tr>

            <tr>
              <td>Objem motora : </td>
              <td>{{ $motorka->objem_motora }}</td>
            </tr>

         	<tr>
              <td>Výkon : </td>
              <td>{{ $motorka->vykon }}</td>
            </tr>

            <tr>
              <td>Oprávnenie: </td>
              <td>{{ $motorka->vodicske_opravnenie }}</td>
            </tr>

            <tr style="font-weight: 900">
              <td>Cenník : </td>
              <td>{{ $motorka->cennik }}</td>
            </tr>

            <tr style="font-weight: 900">
              <td>Kaucia : </td>
              <td>{{ $motorka->kaucia }} EUR</td>
            </tr>

          </table>
          <table class="table" style="margin-top: 30px">
            <tr>
              <td><i class="fa fa-car"></i>  </td>
              <td><i class="fa fa-dashboard"></i>  </td>
              <td><i class="fa fa-cog"></i>  </td>
              <td><i class="fa fa-road"></i>  </td>
              <td class="buttons"><a class="btn btn-theme" href="/motorky/{{$motorka->id}}">Objednaj</a></td>
            </tr>
          </table>
        </div>
      </div>
    </a>