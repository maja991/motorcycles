@extends ('layouts.master')

@section ('content')

<div class="content-area">
<!--    @include('layouts.errors') -->

        <!-- BREADCRUMBS -->
        <section class="page-section breadcrumbs text-center">
            <div class="container">
                <div class="page-header">
                    <h1>{{ $motorka->znacka }} {{ $motorka->model }}</h1>

                </div>
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="/motorky">Motorky</a></li>
                    <li class="active">{{ $motorka->znacka }} {{ $motorka->model }}</li>
                </ul>
            </div>

        </section>
        <!-- /BREADCRUMBS -->

        <!-- PAGE WITH SIDEBAR -->
        <section class="page-section sub-page">


            <div class="container">
                <h2 style="color: black; margin-bottom: 50px">{{ $motorka->popis }}</h2>
                <div class="row">

                    <!-- NOVA GALERIA -->
                    <div class="col-lg-8 col-md-7 col-sm-12">
                        <div class="container">
                            <div class="gallery" style="max-width: 67%">
                            <a href="../storage/images/{{$motorka->images[0]->nazov}}" >
                                    <img src="../storage/images/{{$motorka->images[0]->nazov}}" alt="" title="{{$motorka->images[0]->nazov}}" style="width: 80%; height: 80%; margin-left: 10%; margin-right: 10%">
                                </a>

                                @foreach ($motorka->images as $key => $image)
                                @if($key > 0)
                                <div style="float: left">
                                <a href="../storage/images/{{$image->nazov}}" >
                                    <img src="../storage/images/{{$image->nazov}}" alt="" title="{{$image->nazov}}" class="motocycle_gallery">
                                </a>
                                </div>
                                @endif
                                @endforeach

                            </div>
                        </div>
                    </div>

                    <!-- /NOVA GALERIA -->


                    <div class="col-lg-4 col-md-5 col-sm-7">
                        <div class="project-overview margin_top_A">
                            <h3 class="block-title" ><span>Parametre motorky</span></h3>
                            <table>
							  <tr>
							    <td>typ : </td>
							    <td>{{ $motorka->typ }}</td>
							  </tr>

							  <tr>
							    <td>hmotnosť : </td>
							    <td>{{ $motorka->hmotnost }}</td>
							  </tr>

							  <tr>
							    <td>Maximálna rýchlost : </td>
							    <td>{{ $motorka->max_rychlost }}</td>
							  </tr>

							  <tr>
							    <td>Objem nádrže : </td>
							    <td>{{ $motorka->objem_nadrze }}</td>
							  </tr>

							  <tr>
							    <td>Dojazd : </td>
							    <td>{{ $motorka->dojazd }}</td>
							  </tr>

							</table>



                        </div>

                        <div class="project-details margin_top_A" >
                            <h3 class="block-title"><span>Motor</span></h3>
                            <table>
							  <tr>
							    <td>Typ motora : </td>
							    <td>{{ $motorka->typ_motora }}</td>
							  </tr>

							  <tr>
							    <td>Objem motora : </td>
							    <td>{{ $motorka->objem_motora }}</td>
							  </tr>

                              <tr>
                                <td>Výkon : </td>
                                <td>{{ $motorka->vukon }}</td>
                              </tr>

							</table>
                        </div>

                        <div class="project-details margin_top_A">
                            <h3 class="block-title"><span>Prevodovka</span></h3>
                            <table>
							  <tr>
							    <tr>
							    <td>Prevodovka : </td>
							    <td>{{ $motorka->prevodovka }}</td>
							  </tr>
							</table>
                        </div>

                        <div class="project-details margin_top_A">
                            <h3 class="block-title"><span>Ďalšie parametre</span></h3>
                            <table>
                              <tr>
                                <td>Výška sedadla : </td>
                                <td>{{ $motorka->vyska_sedadla }}</td>
                              </tr>

                              <tr>
                                <td>Vodišské oprávnenie : </td>
                                <td>{{ $motorka->vodicske_opravnenie }}</td>
                              </tr>

                            </table>
                        </div>


                        <div class="project-details margin_top_A">
                            <h3 class="block-title"><span>Cenník</span></h3>
                            <table>

                              <tr>
                                <td>{{ $motorka->cennik }}</td>
                              </tr>

                              <tr>
                                <td>Kaucia : </td>
                                <td>{{ $motorka->kaucia }}</td>
                              </tr>

                            </table>
                        </div>

                        <div class="project-details margin_top_A">
                            {{ $motorka->poznamka }}
                        </div>
                    </div>

                </div>

                <hr class="page-divider"/>
<!--
                <div class="pager">
                    <a class="btn btn-theme btn-theme-transparent pull-left btn-icon-left" href="#"><i class="fa fa-angle-double-left"></i>Older</a>
                    <a class="btn btn-theme btn-theme-transparent pull-right btn-icon-right" href="#">Newer <i class="fa fa-angle-double-right"></i></a>
                </div>


                <hr class="page-divider half"/>
 -->
                <h2 class="block-title">Objednávkový formulár</h2>

                <div class="row thumbnails portfolio">
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail no-border no-padding">
                            <div class="media">
                                <img src="assets/img/preview/portfolio/portfolio-x1.jpg" alt="">
                                <div class="caption hovered">
                                    <div class="caption-wrapper div-table">
                                        <div class="caption-inner div-cell">
                                            <h3 class="caption-title"><a href="#">Project Title</a></h3>
                                            <p class="caption-category"><a href="#">Category</a>, <a href="#">Category 2</a></p>
                                            <p class="caption-buttons">
                                                <a href="assets/img/preview/portfolio/portfolio-x1-big.jpg" class="btn caption-zoom" data-gal="prettyPhoto"><i class="fa fa-search"></i></a>
                                                <a href="portfolio-single.html" class="btn caption-link"><i class="fa fa-link"></i></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail no-border no-padding">
                            <div class="media">
                                <img src="assets/img/preview/portfolio/portfolio-x2.jpg" alt="">
                                <div class="caption hovered">
                                    <div class="caption-wrapper div-table">
                                        <div class="caption-inner div-cell">
                                            <h3 class="caption-title"><a href="#">Project Title</a></h3>
                                            <p class="caption-category"><a href="#">Category</a>, <a href="#">Category 2</a></p>
                                            <p class="caption-buttons">
                                                <a href="assets/img/preview/portfolio/portfolio-x2-big.jpg" class="btn caption-zoom" data-gal="prettyPhoto"><i class="fa fa-search"></i></a>
                                                <a href="portfolio-single.html" class="btn caption-link"><i class="fa fa-link"></i></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail no-border no-padding">
                            <div class="media">
                                <img src="assets/img/preview/portfolio/portfolio-x3.jpg" alt="">
                                <div class="caption hovered">
                                    <div class="caption-wrapper div-table">
                                        <div class="caption-inner div-cell">
                                            <h3 class="caption-title"><a href="#">Project Title</a></h3>
                                            <p class="caption-category"><a href="#">Category</a>, <a href="#">Category 2</a></p>
                                            <p class="caption-buttons">
                                                <a href="assets/img/preview/portfolio/portfolio-x3-big.jpg" class="btn caption-zoom" data-gal="prettyPhoto"><i class="fa fa-search"></i></a>
                                                <a href="portfolio-single.html" class="btn caption-link"><i class="fa fa-link"></i></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail no-border no-padding">
                            <div class="media">
                                <img src="assets/img/preview/portfolio/portfolio-x4.jpg" alt="">
                                <div class="caption hovered">
                                    <div class="caption-wrapper div-table">
                                        <div class="caption-inner div-cell">
                                            <h3 class="caption-title"><a href="#">Project Title</a></h3>
                                            <p class="caption-category"><a href="#">Category</a>, <a href="#">Category 2</a></p>
                                            <p class="caption-buttons">
                                                <a href="assets/img/preview/portfolio/portfolio-x4-big.jpg" class="btn caption-zoom" data-gal="prettyPhoto"><i class="fa fa-search"></i></a>
                                                <a href="portfolio-single.html" class="btn caption-link"><i class="fa fa-link"></i></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- /PAGE WITH SIDEBAR -->

    </div>


    <div class="row">
        <div class="col-lg-3 col-md-3"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 project-media">
            @include ('motorky.objednavka_motorka')
        </div>
    </div>




@endsection