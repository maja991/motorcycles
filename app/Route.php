<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    public function comments(){
    	return $this->hasMany(Comment::class);
    }
    public function images(){
    	return $this->hasMany(Images_route::class);
    }
}
