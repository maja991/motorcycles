<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RegistrationController extends Controller
{
    public function create ()
    {
    	return view('registration.create');
    }

    public function store ()
    {
    	$this->validate(request(),[
    		'name'=>'required',
    		'email'=>'required|email',
    		'password'=>'required|confirmed'
    	]);
    	$user = User::create([ 
	        'name' => request('name'),
	        'email' => request('email'),
	        'password' => bcrypt(request('password')),
            'typ_uzivatela'=>'user',
            'name_' => request('meno'),
            'surname' => request('priezvisko'),
            'telephone' => request('telefon'),
            'gender'=> request('gender_radioInline')

        ]);
    	auth()->login($user);
    	return redirect()->home();
    }
}
