<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Motocycle;
use App\Image;


class MotocyclesController extends Controller
{
    public function index()
    {
    	$motorky = Motocycle::all();
        /*$motorky = Motocycle:: where(function($querry){
            $min_price = Input::has('min_price') ? Input::get('min_price') :null;
            $max_price = Input::has('max_price') ? Input::get('max_price') :null;
        
        if (isset($min_price) && isset($max_price)){
            $querry->where('kaucia', '>=', $min_price)
                   ->where('kaucia', '<=', $max_price);
                   }
        })->get();*/
    	return view('motorky.index', compact('motorky'));

    }

    public function show ($id)
    {
    	$motorka = Motocycle::find($id);
    	return view('motorky.show', compact('motorka', 'obrazky'));
    }
    public function create ()
    {
        return view('motorky.create');
    }
        public function store (Request $request)
    {
        $last_motocycle_id = DB::table('motocycles')->orderBy('id', 'desc')->first()->id;

        if($request->hasFile('images')){
            foreach ($request->images as $image) {
                $filename=$image->GetClientOriginalName();
                $filename=time().'_'.$filename;
                $image->storeAs('public/images',$filename);

                $image = new Image;
                $image->nazov = $filename;
                $image->motocycle_id = $last_motocycle_id+1;
                $image->save();
            }
        }

        $motocycle = new Motocycle;
        $motocycle->id=$last_motocycle_id+1;
        $motocycle->znacka = request('znacka');
//        $motocycle->model = request('model');
        $motocycle->typ = request('typ');
        $motocycle->hmotnost = request('hmotnost');
        $motocycle->typ_motora = request('typ_motora');
        $motocycle->objem_motora = request('objem_motora');
        $motocycle->prevodovka = request('prevodovka');
        $motocycle->objem_nadrze = request('objem_nadrze');
        $motocycle->max_rychlost = request('max_rychlost');
        $motocycle->dojazd = request('dojazd');
        $motocycle->popis = request('popis');
        $motocycle->cennik = request('cennik');
        $motocycle->vykon = request('vykon');
        $motocycle->vyska_sedadla = request('vyska_sedadla');
        $motocycle->vodicske_opravnenie = request('vodicske_opravnenie');
        $motocycle->kaucia = request('kaucia');
        $motocycle->poznamka = request('poznamka');
        $motocycle->save();

        return redirect()->home();
    }
}
