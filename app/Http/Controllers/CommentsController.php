<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Route;
use App\Comment;

class CommentsController extends Controller
{
    public function store(Route $trasa)
    {
    	$comment=New Comment();
    	$comment->komentar=request('komentar');
    	$comment->route_id=$trasa->id;
    	$comment->save(); 
    	return back();
//    	return view('trasy.show', compact('trasa'));   	
    }
}
