<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SessionsController extends Controller
{
    public function create ()
    {
    	return view('sessions.create');
    }

    public function destroy ()
    {
    	auth()->logout();
    	return redirect()->home();
    }

    public function store ()
    {
    	if(! auth()->attempt(request(['email', 'password'])))
    	{
    		return back()->withErrors(['message'=> 'Nesprávny email alebo heslo']);
    	}
	    	if (Auth::user()->typ_uzivatela == 'admin'){
	    		return redirect('/administracia');
	    	}
		return redirect()->home();
    	
    }
}
