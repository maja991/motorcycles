<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Mail\Objednavka;
use App\Mail\Potvrdenie_objednavky;
use DB;
use App\Motocycle;

class OrderFormController extends Controller
{
    public function index ()
    {
        $motorky = Motocycle::all();
        return view('motorky.objednavka', compact('motorky'));

    }
    public function store ()
    {
        $this->validate(request(),[
            'meno'=>'required|min:2',
            'priezvisko'=>'required|min:2',
            'email'=>'required|email',
            'telefon'=>'required|min:10',
            'miesto_pristavenia'=>'required',
            'miesto_vratenia'=>'required',
        ]);

    	$objednavka = new Order;
    	$objednavka->meno = request('meno');
    	$objednavka->priezvisko = request('priezvisko');
    	$objednavka->email = request('email');
    	$objednavka->telefon = request('telefon');
    	$objednavka->adresa_pristavenia = request('miesto_pristavenia');
    	$objednavka->adresa_vratenia = request('miesto_vratenia');
    	$objednavka->id_motorky = request('motorky');
    	$objednavka->prislusenstvo = 'prislusenstvo';
        $objednavka->prislusenstvo1 = request('prislusenstvo1');
        $objednavka->prislusenstvo2 = request('prislusenstvo2');
        $objednavka->prislusenstvo3 = request('prislusenstvo3');
        $objednavka->prislusenstvo4 = request('prislusenstvo4');
        $objednavka->prislusenstvo5 = request('prislusenstvo5');
        $objednavka->prislusenstvo6 = request('prislusenstvo6');
        $objednavka->prislusenstvo7 = request('prislusenstvo7');
        $objednavka->prislusenstvo8 = request('prislusenstvo8');
    	$objednavka->save();



    	//$motorka = DB::select('select * from motocycles where id = ?', [request('motorky')]);
    	$motorka = motocycle::find(request('motorky'));
    	$objednavka->znacka = $motorka->znacka;
//    	$objednavka->model = $motorka->model;

    	\Mail::to(request('email'))->send(new Potvrdenie_objednavky($objednavka));
    	\Mail::to('smartedge.develop@gmail.com')->send(new Objednavka($objednavka));

    	return redirect()->home()->with('alert', 'Objednávka bola odoslaná. Na zadanú e-mailovú adresu bol odoslaný potvrdzovací email.');
    }
}
