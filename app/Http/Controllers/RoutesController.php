<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Route;
use App\Motocycle;
use App\Images_route;

class RoutesController extends Controller
{
    public function index()
    {
    	$trasy = DB::table('routes')->get();
    	return view('trasy.index', compact('trasy'));
    }

    public function show ($id)
    {
    	$trasa = route::find($id);
    	return view('trasy.show', compact('trasa'));
    }    

    public function store_image (Request $request, Route $trasa)
    {
        if($request->hasFile('images')){
            foreach ($request->images as $image) {
                $filename=$image->GetClientOriginalName();
                $filename=time().'_'.$filename;
                $image->storeAs('public/images-route',$filename);

                $image = new Images_route;
                $image->nazov = $filename;
                $image->route_id = $trasa->id;
                $image->save();
            }
        }
        return back();
    }
    public function create ()
    {
        return view('administracia.pridaj_trasu');
    }
    public function store (Request $request)
    {
        $last_route_id = DB::table('routes')->orderBy('id', 'desc')->first()->id;

        if($request->hasFile('images')){
            foreach ($request->images as $image) {
                $filename=$image->GetClientOriginalName();
                $filename=time().'_'.$filename;
                $image->storeAs('public/images-route',$filename);

                $image = new Images_route;
                $image->nazov = $filename;
                $image->route_id = $last_route_id+1;
                $image->save();
            }
        }

        $route = new Route;
        $route->id=$last_route_id+1;
        $route->nazov = request('nazov');
        $route->mapa = request('mapa');
        $route->casova_narocnost = request('casova_narocnost');
        $route->dlzka = request('dlzka');
        $route->save();
        
        return redirect()->home();
    }
}
