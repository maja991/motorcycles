<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Potvrdenie_objednavky extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $objednavka;
    public function __construct($objednavka)
    {
        $this->objednavka = $objednavka;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    //    return $this->markdown('emails.objednavka');
        return $this->from('motocuore@motocuore', 'Motocuore')
            ->subject('Potvrdenie objednávky')
            ->markdown('emails.potvrdenie_objednavky')->with(['objednavka', $this->objednavka]);
    }
}
