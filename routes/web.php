<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MotocyclesController@index')->name('home');
Route::get('/motorky', 'MotocyclesController@index');
Route::get('/motorky/{motorka}', 'MotocyclesController@show');
Route::post('/objednaj', 'OrderFormController@store');
Route::get('/objednavka', 'OrderFormController@index');
Route::get('trasy', 'RoutesController@index');
Route::get('/trasy/{trasa}', 'RoutesController@show');
Route::post('/trasy/{trasa}/komentare', 'CommentsController@store');
Route::get('/o-nas', function () {return view('layouts.o_nas');});
Route::get('kontakt', function () {return view('contact.index');});
Route::get('/administracia/pridaj_motorku','MotocyclesController@create');
Route::post('pridaj_motorku','MotocyclesController@store');
Route::post('/trasy/{trasa}/pridaj_obrazok_trasy','RoutesController@store_image');
Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');
Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');
Route::get('/administracia', function () {return view('administracia.administracia');});
Route::get('/administracia/zoznam_objednavok', 'OrdersController@index');
Route::get('/administracia/zoznam_pouzivatelov', 'UsersController@index');
Route::get('/administracia/pridaj_trasu','RoutesController@create');
Route::post('pridaj_trasu','RoutesController@store');

Route::get('/test', function () {return view('layouts.test');});
